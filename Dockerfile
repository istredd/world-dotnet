FROM mcr.microsoft.com/dotnet/core/sdk:2.2
 
WORKDIR /app
COPY ./World .

RUN ["dotnet", "restore"]
RUN ["dotnet", "build"]

HEALTHCHECK --interval=1m --timeout=3s --retries=5 CMD curl -f http://localhost:5100 || exit 1
ENTRYPOINT ["dotnet", "run"]
