﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace World.Controllers {
    [Route ("/")]
    public class WorldController : ControllerBase {
        [HttpGet]
        public ActionResult<string> Get () {
            return "world";
        }
    }
}